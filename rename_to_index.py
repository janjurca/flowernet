import os
import argparse
import subprocess

parser = argparse.ArgumentParser()
parser.add_argument("--source", action="store", type=str, help="")
parser.add_argument("--species", action="store", type=str, help="")
args = parser.parse_args()

with open(args.species, 'r', encoding="utf-8") as file:
    for i, specie in enumerate(file.read().splitlines()):
        p = args.source.rstrip("/") + '/' + str(i)
        if not os.path.isdir(p):
            os.makedirs(p)
        command = 'mv \'' + args.source.rstrip("/") + '/' + specie +  '\'/* '  +  p
        print(command.encode('latin-1'))
        subprocess.call(command.encode('latin-1') ,shell=True)

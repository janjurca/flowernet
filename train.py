import torch
import torchvision
import torchvision.transforms as transforms
import matplotlib.pyplot as plt
import numpy as np
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import random
from network import load_checkpoint, resnet14, resnet18, resnet34, resnet50, resnet101, ClassifierNet
from imgaug import augmenters as iaa
import imgaug as ia
import matplotlib as mpl
import argparse
import PIL

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

def show_dataset(dataset, n=10):
    img = np.vstack((np.hstack((np.asarray(dataset[i][0]) for _ in range(n)))
                     for i in range(len(dataset))))
    plt.imshow(img)
    plt.axis('off')
    plt.show()

def my_imshow(img):
    img = img / 2 + 0.5     # unnormalize
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))
    plt.show()

class ImgAugTransform:
    def __init__(self):
        self.aug = iaa.Sequential([
            iaa.Sometimes(0.25, iaa.GaussianBlur(sigma=(0, 1.0))),
            iaa.Fliplr(0.5),
            iaa.Affine(rotate=(-20, 20), mode='symmetric'),
            iaa.Sometimes(0.25,
                          iaa.OneOf([iaa.Dropout(p=(0, 0.1)),
                                     iaa.CoarseDropout(0.1, size_percent=0.5)])),
            iaa.AddToHueAndSaturation(value=(-10, 10), per_channel=True)
        ])

    def __call__(self, img):
        img = np.array(img)
        return self.aug.augment_image(img)

def train():
    running_loss = 0.0
    for i, data in enumerate(trainloader):
        # get the inputs
        inputs, labels = data
        inputs, labels = inputs.to(device), labels.to(device)
        # zero the parameter gradients
        optimizer.zero_grad()
        # forward + backward + optimize
        outputs = net(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()

        # print statistics
        running_loss += loss.item()
    print('Epoch: %d, loss: %.3f' % (epoch + 1, running_loss/i ))
    running_loss = 0.0


def test():
    correct = 0
    total = 0
    print("Testing")
    with torch.no_grad():
        for i, data in enumerate(testloader):
            images, labels = data
            images, labels = images.to(device), labels.to(device)
            outputs = net(images)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()
    print('Accuracy of the network on the test images: %d %%' % (100 * correct / total))

    class_correct = list(0. for i in range(args.num_classes))
    class_total = list(0. for i in range(args.num_classes))
    with torch.no_grad():
        for j, data in enumerate(testloader):
            images, labels = data
            images, labels = images.to(device), labels.to(device)
            outputs = net(images)
            _, predicted = torch.max(outputs, 1)
            c = (predicted == labels).squeeze()
            for i in range(len(labels)):
                label = labels[i]
                class_correct[label] += c[i].item()
                class_total[label] += 1

    for i in range(args.num_classes):
        print('Accuracy of %5s : %2d %%' % (classes[i],       100 * class_correct[i] / class_total[i]))


parser = argparse.ArgumentParser()
parser.add_argument("--train", action="store", type=str, help="source file")
parser.add_argument("--test", action="store", type=str, help="source file")
parser.add_argument("--batchsize", action="store",default=8, type=int, help="source file")
parser.add_argument("--num_classes", action="store",default=625, type=int, help="source file")
parser.add_argument("--ui", action="store_true", help="source file")
parser.add_argument("--weights", action="store",default=None, type=str, help="torch wieghts file")
parser.add_argument("--epochs", action="store",default=10, type=int, help="epoch count")
parser.add_argument("--model", action="store",required=False, type=str, help="model to use")
parser.add_argument("--species", action="store",required=False, type=str, help="")
parser.add_argument("--pretrained", action="store_true",required=False, default=False, help="")
parser.add_argument("--test_only", action="store_true",required=False, default=False, help="")
parser.add_argument("--test_epoch", action="store", default=10, help="")
args = parser.parse_args()

#transform = ImgAugTransform()
transform = transforms.Compose([ImgAugTransform(),
    transforms.ToTensor(),
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]
    )

test_transform = transforms.Compose([
    transforms.Resize((256,256)),
    transforms.ToTensor(),
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]
    )

trainset = torchvision.datasets.ImageFolder(args.train, transform=transform)
trainloader = torch.utils.data.DataLoader(trainset, batch_size=args.batchsize, shuffle=True, num_workers=2)

testset = torchvision.datasets.ImageFolder(args.test, transform=test_transform)
testloader = torch.utils.data.DataLoader(testset, batch_size=args.batchsize, shuffle=False, num_workers=2)

classes = []
with open(args.species) as file:
    for specie in file.read().splitlines():
        if specie.strip():
            classes.append(specie)


models = {
"resnet14": resnet14(pretrained=False, num_classes=1000, input_channels=3).to(device),
"resnet18": resnet18(pretrained=args.pretrained, num_classes=1000, input_channels=3).to(device),
"resnet34": resnet34(pretrained=args.pretrained, num_classes=1000, input_channels=3).to(device),
"resnet50": resnet50(pretrained=args.pretrained, num_classes=1000, input_channels=3).to(device),
"resnet101": resnet101(pretrained=args.pretrained, num_classes=1000, input_channels=3).to(device),
}

model = models[args.model]
net = ClassifierNet(args.num_classes,model)
net = net.to(device)
criterion = nn.CrossEntropyLoss()
#optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)
optimizer = optim.Adam(net.parameters(), lr=0.01)

n_parameters = sum([p.data.nelement() for p in net.parameters()])
print('  + Number of params: {}'.format(n_parameters))



if args.weights:
    load_checkpoint(net, optimizer, args.weights)

if args.test_only:
    test()
    exit(0)
for epoch in range(args.epochs):  # loop over the dataset multiple times
    train()
    state = {'epoch': epoch + 1, 'state_dict': net.state_dict(), 'optimizer': optimizer.state_dict() }
    torch.save(state, "flowernet.torch." + str(epoch))
    if epoch % args.test_epoch == (args.test_epoch-1):
        test()
print('Finished Training')
